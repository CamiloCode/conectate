"""
Definition of urls for DjangoWebProject1.
"""

from datetime import datetime
from django.conf.urls import patterns, url, include
from app.forms import BootstrapAuthenticationForm
from django.contrib import admin
from rest_framework import routers, viewsets
from app.models import Ova
from app.serializers import OvaSerializer

admin.autodiscover()


# ViewSets define the view behavior.
class OvaViewSet(viewsets.ModelViewSet):
    queryset = Ova.objects.all()
    serializer_class = OvaSerializer


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'ovas', OvaViewSet)

urlpatterns = patterns('',
                       # Examples:
                       url(r'^$', 'app.views.home', name='home'),
                       url(r'^ova_upload/$', 'app.views.ova_upload', name='ova_upload'),
                       url(r'^main_page/$', 'app.views.main_page', name='main_page'),
                       url(r'^ova_search/$', 'app.views.ova_search', name='ova_search'),
                       url(r'^login/$',
                           'django.contrib.auth.views.login',
                           {
                               'template_name': 'app/login.html',
                               'authentication_form': BootstrapAuthenticationForm,
                               'extra_context':
                                   {
                                       'title': 'Iniciar sesión en Conecta-TE',
                                       'year': datetime.now().year,
                                   }
                           },
                           name='login'),
                       url(r'^logout$',
                           'django.contrib.auth.views.logout',
                           {
                               'next_page': '/',
                           },
                           name='logout'),

                       url(r'^ovas/$', 'app.views.get_ovas'),
                       # Uncomment the admin/doc line below to enable admin documentation:
                       # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

                       # Uncomment the next line to enable the admin:
                       url(r'^admin/', include(admin.site.urls)),
                       # url(r'^search/', 'app.views.ova_search', name='ova_search'),
                       )
