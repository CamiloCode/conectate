"""
Definition of views.
"""

from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from datetime import datetime
from app import services
from app.forms import LomForm
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from app.models import Ova
from app.serializers import OvaSerializer

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    if request.user.is_authenticated():
        return redirect('/main_page')
    return render(
        request,
        'app/index.html',
        context_instance=RequestContext(request,
                                        {
                                            'title': 'Conecta-TE',
                                            'year': datetime.now().year,
                                        })
    )


def ova_search(request):
    if request.method == 'GET':
        return render(
            request,
            'app/search.html',
            context_instance=RequestContext(request,
                                            {
                                                'title': 'Busqueda por palabra clave',
                                            })
        )
    elif request.method == 'POST':
        keyword = request.POST.get('keyword')
        ovas = services.find_ovas_by_keyword(keyword)
        metadata = []
        for ova in ovas:
            metadata.append(services.get_ova_metadata(ova))

        return render(
            request,
            'app/search.html',
            context_instance=RequestContext(request,
                                            {
                                                'title': 'Busqueda por "{}"'.format(keyword),
                                                'year': datetime.now().year,
                                                'keyword': keyword,
                                                'ovas': ovas,
                                                'c': len(ovas),
                                                'metadata': metadata
                                            })
        )


def ova_upload(request):
    if request.method == "GET":
        if request.user.is_authenticated():
            lom_form = LomForm()
            assert isinstance(request, HttpRequest)
            return render(
                request,
                'app/ovaupload.html',
                context_instance=RequestContext(request,
                                                {
                                                    'title': 'Upload your OVA',
                                                    'year': datetime.now().year,
                                                    'lom_form': lom_form,
                                                    'error': False
                                                })
            )
        else:
            return render(
                request,
                'app/ovaupload.html',
                context_instance=RequestContext(request,
                                                {
                                                    'title': 'Upload your OVA',
                                                    'year': datetime.now().year,
                                                    'message': 'No existe usuario logeado en la plataforma.',
                                                    'error': True
                                                })
            )
    elif request.method == "POST":
        lom_form = LomForm(request.POST, request.FILES)
        try:
            '''
            ova_data = Ova()
            ova_data.lom_id = lom_data.id
            ova_data.uploaded_date = datetime.now(tz=None)
            ova_data.uploaded_by_id = request.user.id
            ova_data.file_path = "Testfile.test"
            ova_data.save()
            '''
            services.create_item(lom_form, request.FILES["ova_file"], request.user)
            lom_form = LomForm()
            assert isinstance(request, HttpRequest)
            return render(
                request,
                'app/ovaupload.html',
                context_instance=RequestContext(request,
                                                {
                                                    'title': 'Ova Upload',
                                                    'year': datetime.now().year,
                                                    'lom_form': lom_form,
                                                    'save_status': "Ok",
                                                    'message': 'Ova a sido subido correctamente'
                                                })
            )
        except ValueError:
            print(ValueError)
            return render(
                request,
                'app/ovaupload.html',
                context_instance=RequestContext(request, {
                    'title': 'Ova Upload',
                    'year': datetime.now().year,
                    'lom_form': lom_form,
                    'message': "Fail: " + ValueError
                }))


@login_required
def main_page(request):
    """
    Main page which lists uploaded OVAs
    """
    assert isinstance(request, HttpRequest)
    # table = OvaTable(services.find_ovas_by_keyword('Instrucciones'))
    # table.paginate(page=request.GET.get('page', 1), per_page=5)
    # return render(request, 'app/ova_list.html', {'ovas': table })
    return render(
        request,
        'app/index.html',
        context_instance=RequestContext(request,
                                        {
                                            'title': 'Conecta-TE',
                                            'year': datetime.now().year,
                                        })
    )

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def get_ovas(request):
    """
    List all ovas
    """
    if request.method == 'GET':
        snippets = Ova.objects.all()
        serializer = OvaSerializer(snippets, many=True)
        return JSONResponse(serializer.data)
