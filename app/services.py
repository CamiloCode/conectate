import json
import os
from django.conf import settings
import requests
import slumber
from requests.auth import AuthBase
from app.models import Ova


class ParseAuth(AuthBase):
    def __init__(self, app_id, api_key):
        self.app_id = app_id
        self.api_key = api_key

    def __call__(self, r):
        token = login_dspace("admin@dspace-inception.tk", "dspace")
        r.headers['rest-dspace-token'] = token
        return r


def login_dspace(user, password):
    dspace = slumber.API(settings.DSPACE_URL)
    token = dspace.login.post({"email": user, "password": password})
    return token


def get_collections():
    dspace = slumber.API(settings.DSPACE_URL)
    collections_rest = dspace.collections.get()
    collections = []
    for item in collections_rest:
        collection = (int(item["id"]), item["name"])
        collections.append(collection)
    return collections


def find_ovas_by_keyword(keyword):
    """Finds OVAs in DSpace by keyword and returns a list with the id and title."""
    dspace = slumber.API(settings.DSPACE_URL)
    items = dspace.items('find-by-metadata-field').post(
        {"key": "lom.general.keyword", "value": keyword, "language": "en_US"})
    ids = []
    for item in items:
        ids.append(item['id'])

    items = dspace.items('find-by-metadata-field').post(
        {"key": "lom.general.keyword", "value": keyword, "language": ""})
    for item in items:
        # if(Ova.objects)
        ids.append(item['id'])

    return list(set(ids))


def get_ova_metadata(id):
    dspace = slumber.API(settings.DSPACE_URL)
    metadata = dspace.items(id).metadata.get()
    datos = {}
    datos["id"]=id

    for meta in metadata:
        print("meta: {}".format(meta))
        if meta['key'] == 'lom.general.title':
            datos['title'] = meta['value']
        if meta['key'] == 'lom.general.description':
            datos['description'] = meta['value']
        if meta['key'] == 'dc.date.available':
            datos['date'] = meta['value']
        if meta['key'] == 'lom.general.keyword':
            if 'keyword' not in datos:
                datos['keyword'] = []
            arreglo_keywork = datos['keyword']
            arreglo_keywork.append(meta['value'])
            datos['keyword'] = arreglo_keywork

    print("datos: {}".format(datos))
    return datos

def create_item(ova_item, file_req, user):
    s = requests.Session()
    s.verify = False
    s.headers.update({'rest-dspace-token': login_dspace("admin@dspace-inception.tk", "dspace")})
    dspace = slumber.API(settings.DSPACE_URL, session=s)
    item = dspace.collections(1).items.post(ova_item.get_json_item())
    ova_id_dspace = item["id"]
    ova = Ova(dspace_id=ova_id_dspace, is_visible=False, state=0, uploaded_by=user, edited_by=None)
    ova.save()
    ova_metadata_json = json.dumps(ova_item.get_json_metadata())
    dspace.items(ova_id_dspace).metadata().post(json.loads(ova_metadata_json))
    '''
    with open('./{}'.format(file_req.name), "rb", buffering=0) as fp:
        dspace.items(ova_id_dspace).bitstreams().post(
            {"name": file_req.name, "mimeType": file_req.content_type, "format": file_req.content_type,
             "sizeBytes": file_req.size}, files={"file": fp.read()})
    fp.close()
    os.remove('./{}'.format(file_req.name))
    '''
