# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.auth.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('comment', models.TextField()),
                ('date_comment', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='ConectateUser',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, to=settings.AUTH_USER_MODEL, primary_key=True)),
                ('is_admin', models.BooleanField()),
                ('is_super_editor', models.BooleanField()),
                ('is_professor', models.BooleanField()),
                ('is_editor', models.BooleanField()),
                ('can_download', models.BooleanField()),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Ova',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('dspace_id', models.IntegerField()),
                ('is_visible', models.BooleanField()),
                ('state', models.IntegerField(choices=[(0, 'without editor'), (1, 'to approve'), (2, 'rejected'), (3, 'approved')])),
                ('edited_by', models.ForeignKey(related_name='edited_by', null=True, to=settings.AUTH_USER_MODEL)),
                ('uploaded_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='uploaded_by')),
            ],
        ),
        migrations.AddField(
            model_name='comment',
            name='ova_id',
            field=models.ForeignKey(to='app.Ova'),
        ),
        migrations.AddField(
            model_name='comment',
            name='user_id',
            field=models.ForeignKey(to='app.ConectateUser'),
        ),
    ]
