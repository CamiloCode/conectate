"""
Definition of forms.
"""
from datetime import datetime
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from app.services import get_collections

'''
Predominant mode of learning supported by this learning object.
"Active" learning (e.g., learning by doing) is supported
by content that directly induces productive action by the learner. An active learning object prompts the learner for
semantically meaningful input or for some other kind of productive action or decision, not necessarily performed within
the learning object's framework. Active documents include simulations, questionnaires, and exercises.
"Expositive" learning (e.g., passive learning) occurs when the learner's job mainly consists of absorbing the content
exposed to him (generally through text, images or sound). An expositive learning object displays information but does
not prompt the learner for any semantically meaningful input. Expositive documents include essays, video clips,
all kinds of graphical material, and hypertext documents.
When a learning object blends the active and expositive interactivity types, then its interactivity type is "mixed".
'''
ACTIVE = 0
EXPOSITIVE = 1
MIXED = 3
INTERACTIVITY = (
    (ACTIVE, 'active'),
    (EXPOSITIVE, 'expositive'),
    (MIXED, 'mixed')
)

'''
Related to LOM-CO when using the Life Cycle, Status option,
The completion status or condition of this
learning object.
'''
DRAFT = 0
FINAL = 1
REVISED = 2
UNAVAILABEL = 3
STATUS = (
    (DRAFT, 'draft'),
    (FINAL, 'final'),
    (REVISED, 'revised'),
    (UNAVAILABEL, 'unavailable')
)

'''
The degree of interactivity characterizing this learning object. Interactivity in this context refers to the degree to
which the learner can influence the aspect or behavior of the learning object .
'''
VERYLOW = 0
LOW = 1
MEDIUM = 2
HIGH = 3
VERYHIGH = 4
INTERACTIVITY_LEVEL = (
    (VERYLOW, 'very low'),
    (LOW, 'low'),
    (MEDIUM, 'medium'),
    (HIGH, 'high'),
    (VERYHIGH, 'very high')
)

'''
Principal user(s) for which this learning object was designed, most dominant first.
'''
TEACHER = 0
STUDENT = 1
STUDENT_TYPE = (
    (TEACHER, 'teacher'),
    (STUDENT, 'student')
)

'''
Education level
'''
PRIMARY = 0
SECONDARY = 1
BACHELOR = 2
MASTERS = 3
DOCTORATE = 4
EDUCATION_LEVEL = (
    (PRIMARY, 'primary'),
    (SECONDARY, 'secondary'),
    (BACHELOR, 'bachelor'),
    (MASTERS, 'masters'),
    (DOCTORATE, 'doctorate')
)

'''
Language
'''
ENGLISH_US = 0
SPANISH_CO = 1
LANGUAGE = (
    (ENGLISH_US, 'en_US'),
    (SPANISH_CO, 'es_CO')
)


class BootstrapAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': 'Nombre de usuario'}))
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput({
                                   'class': 'form-control',
                                   'placeholder': 'Contraseña'}))


class LomForm(forms.Form):
    """
    Model that describes a LOM-CO
    """
    # Name given to this learning object.
    general_title = forms.CharField(max_length=256)
    general_language = forms.ChoiceField(choices=LANGUAGE)
    # The primary human language or languages used within this learning object to communicate to the intended user.
    # A textual description of the content of this learning object.
    general_description = forms.CharField(widget=forms.Textarea)
    # A keyword or phrase describing the topic of this learning object.
    general_keyword = forms.CharField(max_length=256)
    # Parent collection for example: Bellas Artes
    general_collection = forms.ChoiceField(choices=get_collections())
    # The edition of this learning object.
    lifecycle_version = forms.CharField(max_length=256)
    # The completion status or condition of this learning object.
    lifecycle_status = forms.ChoiceField(choices=STATUS)
    # Those entities (i.e., people, organizations) that have contributed to the state of this learning object
    # during its life cycle (e.g.,creation, edits, publication).
    lifecycle_contribute = forms.CharField(max_length=256)
    # The identification of and information about entities (i.e., people, organizations) contributing to this learning
    # object. The entities shall be ordered as most relevant first
    lifecycle_entity = forms.CharField(max_length=256)
    # The date of the contribution.
    lifecycle_date = forms.DateField()
    # Technical datatype(s) of (all the components of) this learning object. Example: "video/mpeg"
    technical_format = forms.CharField(max_length=256)
    # The size of the digital learning object in bytes (octets). The size is represented as a decimal value (radix 10).
    # Consequently, only the digits "0" through "9" should be used. The unit is bytes, not Mbytes, GB, etc.
    technical_size = forms.DecimalField(max_digits=10, decimal_places=2)
    # A string that is used to access this learning object. It may be a location (e.g., Universal Resource Locator),
    # or a method that resolves to a location (e.g., Universal ResourceIdentifier).
    technical_location = forms.CharField(max_length=256)
    # The technical capabilities necessary for using this learning object.
    technical_installationinstructions = forms.CharField(widget=forms.Textarea)
    # Predominant mode of learning supported by this learning object.
    educational_typeofinteractivity = forms.ChoiceField(choices=INTERACTIVITY)
    # Specific kind of learning object. The most dominant kind shall be first.
    educational_resourcetype = forms.CharField(max_length=256)
    # The degree of interactivity characterizing this learning object. Interactivity in this context refers to the
    # degree to which the learner can influence the aspect or behavior of the learning object .
    educational_interactivitylevel = forms.ChoiceField(choices=INTERACTIVITY_LEVEL)
    # Principal user(s) for which this learning object was designed, most dominant first.
    educational_roleuser = forms.ChoiceField(choices=STUDENT_TYPE)
    # Eductaion level target for the learning object.
    educational_level = forms.ChoiceField(choices=EDUCATION_LEVEL)
    # Whether use of this learning object requires payment.
    rights_cost = forms.BooleanField(initial=False)
    # Whether copyright or other restrictions apply to the use of this learning object.
    rights_copyrightandotherrestrictions = forms.BooleanField(initial=False)
    # The name of the classification system.
    classification_source = forms.CharField(max_length=256)
    # A particular term within a taxonomy. Ataxon is a node that has a defined label or term. A taxon may also have an
    # alphanumeric designation or identifier for standardized reference. Either or both the label and the entry may be
    # used to designate a particular taxon.
    classification_taxon = forms.CharField(max_length=256)
    # Uploaded File
    ova_file = forms.FileField()

    def get_json_item(self):
        data = {"name": self.data["general_title"],
                "type": "item",
                "lastModified": str(datetime.now().isoformat())
                }

        return data

    def get_json_metadata(self):
        data = [{"key": "lom.general.title", "value": self.data["general_title"], "language": ""},
                {"key": "lom.general.language", "value": self.data["general_language"], "language": ""},
                {"key": "lom.general.description", "value": self.data["general_description"], "language": ""},
                {"key": "lom.general.keyword", "value": self.data["general_keyword"], "language": ""},

                {"key": "lom.lifecycle.version", "value": self.data["lifecycle_version"], "language": ""},
                {"key": "lom.lifecycle.contribute-date", "value": str(self.data["lifecycle_date"]),
                 "language": ""},
                {"key": "lom.lifecycle.contribute-entity", "value": self.data["lifecycle_entity"], "language": ""},
                {"key": "lom.lifecycle.contribute-role", "value": self.data["lifecycle_contribute"], "language": ""},
                {"key": "lom.technical.format", "value": self.data["technical_format"], "language": ""},
                {"key": "lom.technical.installationremarks", "value": self.data["technical_installationinstructions"],
                 "language": ""},
                {"key": "lom.technical.location", "value": self.data["technical_location"], "language": ""},
                {"key": "lom.technical.requirement-orcomponent-maximumversion", "value": "", "language": ""},
                {"key": "lom.technical.requirement-orcomponent-minimumversion", "value": "", "language": ""},
                {"key": "lom.technical.requirement-orcomponent-name", "value": "", "language": ""},
                {"key": "lom.technical.requirement-orcomponent-type", "value": self.data["technical_format"],
                 "language": ""},
                {"key": "lom.technical.size", "value": str(self.data["technical_size"]), "language": ""},
                {"key": "lom.educational.context", "value": self.data["educational_resourcetype"], "language": ""},
                {"key": "lom.educational.intendedenduserrole", "value": self.data["educational_roleuser"],
                 "language": ""},
                {"key": "lom.educational.interactivitylevel", "value": self.data["educational_interactivitylevel"],
                 "language": ""},
                {"key": "lom.educational.interactivitytype", "value": self.data["educational_typeofinteractivity"],
                 "language": ""},
                {"key": "lom.rights.cost", "value": ("true" if self.data["rights_cost"] == 'on' else "false"), "language": ""},
                {"key": "lom.classification.taxonpath-source", "value": self.data["classification_source"],
                 "language": ""},
                {"key": "lom.classification.taxonpath-taxon-entry", "value": self.data["classification_taxon"],
                 "language": ""},
                {"key": "lom.annotation.description", "value": self.data["general_description"], "language": ""}
                ]
        return data

'''
# {"key":"lom.general.collection", "value": self.data["general_collection.name,"language": ""},
# {"key":"lom.lifecycle.status", "value": self.data["lifecycle_status,"language": ""},
# {"key": "lom.rights.copyrightandotherrestrictions", "value": self.data["rights_copyrightandotherrestrictions"], "language": ""},
# {"key":"lom.educational.level", "value": self.data["educational_level,"language": ""},
'''
