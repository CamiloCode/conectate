from test import test_support
from unittest import TestCase
from app import services

class Test(TestCase):

    def test_get_ova_metadata(self):
        idtest = 5
        result = services.get_ova_metadata(idtest)
        self.assertEqual(idtest, result['id'], "el id del ova deberia ser 5")
        self.assertEqual("Instrucciones", result['title'], "el titulo del ova deberia ser 'Instrucciones'")
        self.assertEqual("Instructivo instalacion DSpace", result['description'], "la descripcion del ova deberia ser 'Instructivo instalacion DSpace'")
        self.assertEqual("2015-09-13T16:45:33Z", result['date'], "la fecha del ova deberia ser '2015-09-13T16:45:33Z'")
        self.assertEqual(["Instrucciones", "DSpace"], result['keyword'], "el keyword del ova deberia ser 'Instrucciones'")