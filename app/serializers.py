from rest_framework import serializers
from app.models import Ova


class OvaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ova
        fields = ('uploaded_by', 'file_path', 'lom_id', 'uploaded_date')