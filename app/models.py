import datetime
from django.db import models
from django.contrib.auth.models import User
from django_tables2 import tables, columns
import itertools

class ConectateUser(User):
    """
        User Model
    """
    def get_full_name(self):
        return self.first_name + " " + self.last_name

    def get_short_name(self):
        return self.first_name

    is_admin = models.BooleanField()
    is_super_editor = models.BooleanField()
    is_professor = models.BooleanField()
    is_editor = models.BooleanField()
    can_download = models.BooleanField()

WITHOUT_EDITOR = 0
TO_APPROVE = 1
REJECTED = 2
APPROVED = 3
OVA_STATE = (
    (WITHOUT_EDITOR, "without editor"),
    (TO_APPROVE, "to approve"),
    (REJECTED, "rejected"),
    (APPROVED, "approved")
)

class Ova(models.Model):
    dspace_id = models.IntegerField()
    is_visible = models.BooleanField()
    state = models.IntegerField(choices=OVA_STATE)
    uploaded_by = models.ForeignKey(User, related_name="uploaded_by")
    edited_by = models.ForeignKey(User, related_name="edited_by", null=True)

class Comment(models.Model):
    comment = models.TextField()
    date_comment = models.DateTimeField()
    user_id = models.ForeignKey(ConectateUser)
    ova_id = models.ForeignKey(Ova)

class MetadataEntry:
    """
    DSpace REST API MetadataEntry Object
    """
    key = models.TextField()
    value = models.TextField()
    language = models.TextField()

class OvaTable(tables.Table):
    row_number = columns.Column(verbose_name=' ', empty_values=(), sortable=False)
    id = columns.Column(visible=False)
    title = columns.Column(verbose_name='Título', sortable=False)

    def __init__(self, *args, **kwargs):
        super(OvaTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return '%d    ' % (next(self.counter) + 1)

    class Meta:
        attrs = {"class": "paleblue"}
